package com.payroll.businessLogic;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeCard {
    public static final double OVERTIME_THRESHOLD = 8.0;
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

    private BusinessCalendar businessCalendar = new TaiwanBusinessCalendar();

    public String name;
    private final Date checkinDate;
    private final Date checkoutDate;
    private int baseHours;
    private int OTHours;

    public void setBusinessCalendar(BusinessCalendar bCalendar) {
        this.businessCalendar = bCalendar;
    }

    public TimeCard(String checkinDateString, String checkoutDateString) {
        this("Unknown", TimeUtils.buildDate(checkinDateString), TimeUtils.buildDate(checkoutDateString));
    }

    public TimeCard(String name, String checkinDateString, String checkoutDateString) {
        this(name, TimeUtils.buildDate(checkinDateString), TimeUtils.buildDate(checkoutDateString));
    }

    public TimeCard(String name, Date checkinDate, Date checkoutDate) {
        this.name = name;
        this.checkinDate = checkinDate;
        this.checkoutDate = checkoutDate;
    }

    public TimeCard(Date checkinDate, Date checkoutDate) {
        this.name = name;
        this.checkinDate = checkinDate;
        this.checkoutDate = checkoutDate;
    }

    public Date getCheckinDate() {
        return (Date) this.checkinDate.clone();
    }

    public Date getCheckoutDate() {
        return (Date) this.checkoutDate.clone();
    }

    public void process() {
        if (!TimeUtils.isDateEarlierThan(this.checkinDate, this.checkoutDate))
            throw new IllegalArgumentException("'check in date can not great or equal to 'check out date'");
        long totalTimeInMs = TimeUtils.compareDate(this.checkinDate, this.checkoutDate);

        // lunch break
        if (TimeUtils.isTimeBefore(this.checkinDate, 12) && TimeUtils.isTimeAfter(this.checkoutDate, 13)) {
            totalTimeInMs = totalTimeInMs - (3600 * 1000);
        }
        int totalHours = (int) (totalTimeInMs / 1000 / 3600);

        if (this.businessCalendar.isNationalHoliday(this.checkinDate)) {
            this.baseHours = 0;
            this.OTHours = totalHours;
        } else {
            this.baseHours = (int) Math.min(OVERTIME_THRESHOLD, totalHours);
            this.OTHours = (totalHours > OVERTIME_THRESHOLD) ? (int) (totalHours - OVERTIME_THRESHOLD) : 0;
        }
        /*
        this.baseHours = (int) Math.min(OVERTIME_THRESHOLD, totalHours);
        this.OTHours = (totalHours > OVERTIME_THRESHOLD) ? (int) (totalHours - OVERTIME_THRESHOLD) : 0;
        */
    }

    public int getBaseHours() {
        return baseHours;
    }

    public void setBaseHours(int baseHours) {
        this.baseHours = baseHours;
    }

    public int getOTHours() {
        return OTHours;
    }

    public void setOTHours(int oTHours) {
        OTHours = oTHours;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
