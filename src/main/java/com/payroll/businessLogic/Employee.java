package com.payroll.businessLogic;

public abstract class Employee {
    private String name;

    public Employee(String name) {
        if (name == null) {
            throw new IllegalArgumentException("name of employee can not be null");
        }

        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public abstract double calculatePay();

    public abstract boolean checkLimitExceed();
}
