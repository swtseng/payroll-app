package com.payroll.servlet;

import com.payroll.businessLogic.HourlyEmployee;
import com.payroll.businessLogic.TimeCard;
import com.payroll.dataAccess.EmployeeDao;
import com.payroll.dataAccess.TimeCardDao;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

@WebServlet(name = "TimeCardServlet", urlPatterns = {"timeCard"}, loadOnStartup = 2)
public class TimeCardServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // get time cards
        ArrayList<TimeCard> timeCards = TimeCardDao.getInstance().getTimeCards();
        request.setAttribute("timeCards", timeCards);

        // page rendering
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("timeCard.jsp");
        requestDispatcher.forward(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // try to parse from parameters
        String employee = null;
        Date checkInTime = null, checkOutTime = null;
        try {
            employee = request.getParameter("employee");
            String checkInDate = request.getParameter("checkInDate");
            String checkInHour = request.getParameter("checkInHour");
            String checkOutDate = request.getParameter("checkOutDate");
            String checkOutHour = request.getParameter("checkOutHour");

            DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            checkInTime =  df.parse(String.format("%s %s:00:00", checkInDate, checkInHour));
            checkOutTime =  df.parse(String.format("%s %s:00:00", checkOutDate, checkOutHour));
        } catch (Exception ex) {
            request.setAttribute("isError", true);
            request.setAttribute("errorCause", ex.toString());
        }

        // insert time card
        TimeCard timeCard = new TimeCard(employee, checkInTime, checkOutTime);
        TimeCardDao.getInstance().insertTimeCard(timeCard);

        // get time cards
        ArrayList<TimeCard> timeCards = TimeCardDao.getInstance().getTimeCards();
        request.setAttribute("timeCards", timeCards);

        // page rendering
        request.setAttribute("isSuccess", true);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("timeCard.jsp");
        requestDispatcher.forward(request, response);
    }
}