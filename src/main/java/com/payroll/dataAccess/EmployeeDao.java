package com.payroll.dataAccess;

import com.payroll.businessLogic.HourlyEmployee;
import com.payroll.businessLogic.TimeCard;

import java.util.ArrayList;

public class EmployeeDao {
    private static ArrayList<HourlyEmployee> hourlyEmployees;

    private static EmployeeDao instance  = new EmployeeDao();

    private EmployeeDao() {
        try {
            hourlyEmployees = new ArrayList<HourlyEmployee>();

            HourlyEmployee bob = new HourlyEmployee("Bob", 120);
            bob.addTimeCard(new TimeCard(TimeCard.DATE_FORMAT.parse("2018/01/01 10:00:00"), TimeCard.DATE_FORMAT.parse("2018/01/01 23:00:00")));
            bob.addTimeCard(new TimeCard(TimeCard.DATE_FORMAT.parse("2018/01/02 10:00:00"), TimeCard.DATE_FORMAT.parse("2018/01/02 12:00:00")));

            hourlyEmployees.add(bob);


            hourlyEmployees.add(new HourlyEmployee("John", 130));
            hourlyEmployees.add(new HourlyEmployee("Mary", 140));
        } catch (Exception ex) {
        }
    }

    public static EmployeeDao getInstance() {
        return instance;
    }

    public ArrayList<HourlyEmployee> getHourlyEmployees() {
        return hourlyEmployees;
    }
}