package com.payroll.dataAccess;

import com.payroll.businessLogic.TimeCard;

import java.util.ArrayList;

public class TimeCardDao {
    private static ArrayList<TimeCard> timeCards;

    private static TimeCardDao instance  = new TimeCardDao();

    private TimeCardDao(){
        timeCards = new ArrayList<TimeCard>();
    }

    public static TimeCardDao getInstance() {
        return instance;
    }

    public ArrayList<TimeCard> getTimeCards() {
        return timeCards;
    }

    public void insertTimeCard(TimeCard timeCard) {
        timeCards.add(timeCard);
    }
}
